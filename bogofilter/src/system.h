/*****************************************************************************

NAME:
   system.h -- system definitions and prototypes for bogofilter

   WARNING: be sure to include this - or config.h - early, because
   some systems may otherwise #define macros from it.

   For instance, OpenIndiana defines _FILE_OFFSET_BITS unless we do.

******************************************************************************/

/* parts were taken from autoconf.info */

#ifndef SYSTEM_H
#define SYSTEM_H

#include "config.h"
#include "bftypes.h"

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include <limits.h>

#if HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif

#include <signal.h>

#include <string.h>

#if HAVE_STRINGS_H
#include <strings.h>
#endif

#ifndef HAVE_STRLCPY
size_t strlcpy(/*@out@*/ char *dst, const char *src, size_t size);
#endif

#ifndef HAVE_STRLCAT
size_t strlcat(/*@out@*/ char *dst, const char *src, size_t size);
#endif

#include <time.h>
#if HAVE_SYS_TIME_H
# include <sys/time.h>
#endif

#if HAVE_FCNTL_H
#include <fcntl.h>
#endif

/* dirent.h surroundings */
/* from autoconf.info */
#if HAVE_DIRENT_H
# include <dirent.h>
# define NAMLEN(dirent) strlen((dirent)->d_name)
#else
# define dirent direct
# define NAMLEN(dirent) (dirent)->d_namlen
# if HAVE_SYS_NDIR_H
#  include <sys/ndir.h>
# endif
# if HAVE_SYS_DIR_H
#  include <sys/dir.h>
# endif
# if HAVE_NDIR_H
#  include <ndir.h>
# endif
#endif

#ifdef __DGUX__
#undef EX_OK
#endif

/* Ignore __attribute__ if not using GNU CC */
#if	!defined(__GNUC__) && !defined(__attribute__)
#define __attribute__(a)
#endif

/* system.c - function prototypes */

extern bool bf_abspath(const char *path);
extern int  bf_mkdir(const char *path, mode_t mode);
extern void bf_sleep(long delay);

#endif /* SYSTEM_H */
