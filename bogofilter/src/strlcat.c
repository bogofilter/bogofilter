/*  strlcat.c - length-limited strcat/strncat alternative from BSD API.
    Copyright (C) 2022  Matthias Andree

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <sys/types.h>
#include <string.h>
#include "system.h"

/*
   This is a reimplementation that defers to 
   standard string functions, on the assumption that libc
   strlen/strcpy/memcpy might be heavily optimized
   and using SIMD instructions (SSE, AVX) or CPU/compiler intrinsic
   string functions.
*/

/*
 * Appends src to string dst of size siz (unlike strncat, siz is the
 * full size of dst, not space left).  At most siz-1 characters
 * will be copied.  Always NUL terminates (unless siz == 0).
 * Returns strlen(src) + strlen(dst); if retval >= siz, truncation occurred.
 */
size_t
strlcat(char *dst, const char *src, size_t siz)
{
	size_t srclen = strlen(src);
	size_t dstlen = 0;
	size_t retval;
	
	if (siz) {
		const char *tmp = (const char *)memchr(dst, '\0', siz); /* strlen() might run off the end of dst if it contains garbage*/
		if (tmp) dstlen = tmp - dst; else dstlen = siz;
	}
	
	retval = srclen + dstlen;

	if (siz && dstlen < siz) { /* only touch anything if there is a buffer with room left */
		if (srclen >= siz - dstlen) srclen = siz - dstlen - 1;
		memcpy(dst + dstlen, src, srclen);
		dst[dstlen + srclen] = '\0';
	}
	return retval;
}
