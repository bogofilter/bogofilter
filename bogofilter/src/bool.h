/*****************************************************************************

NAME:
   bool.h -- prototypes for bool.c

******************************************************************************/

#ifndef	BOOL_H_INCLUDED
#define	BOOL_H_INCLUDED

/* Function prototypes */

extern bool str_to_bool(const char *str);

#endif	/* BOOL_H_INCLUDED */
