/*****************************************************************************

NAME:
   robx.h -- prototypes and definitions for robx.c

******************************************************************************/

#ifndef	ROBX_H_INCLUDED
#define	ROBX_H_INCLUDED

double compute_robinson_x(void);

#endif	/* ROBX_H_INCLUDED */
