/*****************************************************************************

NAME:
   degen.h -- prototypes and definitions for degen.c

******************************************************************************/

#ifndef	DEGEN_H_INCLUDED
#define	DEGEN_H_INCLUDED

#include "word.h"

extern	bool	first_match;
extern	bool	degen_enabled;

double degen(const word_t *token, wordcnts_t *cnts);

#endif	/* DEGEN_H_INCLUDED */
