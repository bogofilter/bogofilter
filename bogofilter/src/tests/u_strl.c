#include "system.h"
#include <string.h>

#undef NDEBUG
#include <assert.h>

/* BEGIN Todd C. Miller/OpenBSD-copyrighted material,
   with strlcat -> ostrlcat
https://cvsweb.openbsd.org/cgi-bin/cvsweb/src/lib/libc/string/strlcat.c?rev=1.19&content-type=text/x-cvsweb-markup
*/
/*	$OpenBSD: strlcat.c,v 1.19 2019/01/25 00:19:25 millert Exp $	*/

/*
 * Copyright (c) 1998, 2015 Todd C. Miller <millert@openbsd.org>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <string.h>

/*
 * Appends src to string dst of size dsize (unlike strncat, dsize is the
 * full size of dst, not space left).  At most dsize-1 characters
 * will be copied.  Always NUL terminates (unless dsize <= strlen(dst)).
 * Returns strlen(src) + MIN(dsize, strlen(initial dst)).
 * If retval >= dsize, truncation occurred.
 */
static size_t
ostrlcat(char *dst, const char *src, size_t dsize)
{
	const char *odst = dst;
	const char *osrc = src;
	size_t n = dsize;
	size_t dlen;

	/* Find the end of dst and adjust bytes left but don't go past end. */
	while (n-- != 0 && *dst != '\0')
		dst++;
	dlen = dst - odst;
	n = dsize - dlen;

	if (n-- == 0)
		return(dlen + strlen(src));
	while (*src != '\0') {
		if (n != 0) {
			*dst++ = *src;
			n--;
		}
		src++;
	}
	*dst = '\0';

	return(dlen + (src - osrc));	/* count does not include NUL */
}
/* END of Todd C. Miller/OpenBSD-copyrighted material */

int main(void) 
{
	const char a[] = "foo";
	const char b[] = "barz";
	char out[8];

	*out = 0x55;
	assert(0 == strlcpy(out, "", 0) && 0x55 == *out);
	assert(strlen(a) == strlcpy(out, a, 0) && 0x55 == *out);
	assert(0 == strlcpy(out, "", 1) && 0x00 == *out);
	assert(strlen(a) == strlcpy(out, a, 3) && 0 == strcmp(out, "fo"));
	assert(strlen(a) == strlcpy(out, a, sizeof out) && 0 == strcmp(out, a));

	/* check bug-for-bug-compatibility with OpenBSD's strlcat(3), ostrlcat */
	strcpy(out, b); assert(3 == ostrlcat(out, "", 3) && 0 == strcmp(out, b)); /* don't tamper with buffer */
	strcpy(out, b); assert(3 ==  strlcat(out, "", 3) && 0 == strcmp(out, b)); /* don't tamper with buffer */
	strcpy(out, b); assert(4 == ostrlcat(out, "", 4) && 0 == strcmp(out, b)); /* don't tamper with buffer */
	strcpy(out, b); assert(4 ==  strlcat(out, "", 4) && 0 == strcmp(out, b)); /* don't tamper with buffer */
	strcpy(out, b); assert(0 == ostrlcat(out, "", 0) && 0 == strcmp(out, b));
	strcpy(out, b); assert(0 ==  strlcat(out, "", 0) && 0 == strcmp(out, b));
	strcpy(out, b); assert(4 == ostrlcat(out, "", sizeof out) && 0 == strcmp(out, b)); /* don't tamper with buffer */
	strcpy(out, b); assert(4 ==  strlcat(out, "", sizeof out) && 0 == strcmp(out, b)); /* don't tamper with buffer */
	strcpy(out, b); assert(7 == ostrlcat(out, a, sizeof out) && 0 == strcmp(out, "barzfoo"));
	strcpy(out, b); assert(7 ==  strlcat(out, a, sizeof out) && 0 == strcmp(out, "barzfoo"));
	strcpy(out, b); assert(7 == ostrlcat(out, a, 5) && 0 == strcmp(out, "barz"));
	strcpy(out, b); assert(7 ==  strlcat(out, a, 5) && 0 == strcmp(out, "barz"));
	strcpy(out, b); assert(7 == ostrlcat(out, a, 6) && 0 == strcmp(out, "barzf"));
	strcpy(out, b); assert(7 ==  strlcat(out, a, 6) && 0 == strcmp(out, "barzf"));
	*out = 0; assert(3 == ostrlcat(out, a, 3) && 0 == strcmp(out, "fo"));
	*out = 0; assert(3 ==  strlcat(out, a, 3) && 0 == strcmp(out, "fo"));
	*out = 0; assert(3 == ostrlcat(out, a, sizeof out) && 0 == strcmp(out, "foo"));
	*out = 0; assert(3 ==  strlcat(out, a, sizeof out) && 0 == strcmp(out, "foo"));
	return 0;
}
